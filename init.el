;;;; Load my configuration modules

;;; Exit emacs: C^x C^c

;; Modules are in ~/.emacs.d/modules
(setq jgy-modules-dir (expand-file-name "modules" user-emacs-directory))
(add-to-list 'load-path jgy-modules-dir)

(load "clean-emacs")
(load "packages")
(load "sessions")
(load "buffer-navigation")
(load "file-navigation")
(load "keybindings")
(load "languages")
(load "display")
(load "emacs-server")
