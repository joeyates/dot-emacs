# dot-emacs

My Emacs configuration.

# Layout

* modules - config split by function
* snippets - autocomplete snippets for various languages

# Modules

Modules are loaded in a specific order by `init.el`.

See inidivdual modules for documentation.
