;;;; Open all files in one instance

;; Skip if we are editing a file without a session
(unless (getenv "EMACS_NO_SESSION") (server-start))
