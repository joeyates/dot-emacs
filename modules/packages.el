;;;; Set up emacs package sources

(require 'package)

;;; M-x list-packages
;;; C^s = search
;;; i = mark for installation
;;; x = execute

(setq package-archives '())

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/") t)

;; activate packages manually
(setq package-enable-at-startup nil)

;; Make sure to have downloaded archive descriptions.
;; TODO: update if stale
(or (file-exists-p package-user-dir)
    (package-refresh-contents))

;; Activate installed packages
(package-initialize)

;; http://stackoverflow.com/questions/10092322/how-to-automatically-install-emacs-packages-by-specifying-a-list-of-package-name/10095853#10095853
(defun ensure-package-installed (&rest packages)
  "Check each package to see if it is installed, ask to install missing.

  Return a list of packages which are installed for every skipped package."
  (mapcar
   (lambda (package)
     (if (package-installed-p package)
         nil
       (if (y-or-n-p (format "Package %s is missing. Install it? " package))
         (progn
           (package-refresh-contents)
           (package-install package))
         package)))
   packages))
