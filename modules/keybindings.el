;;;; Set up global keybindings

;;; C-h v - describe variable
;;;   allows autocomplete

;;; evil-mode - Vim-like keybindings for navigation and editing

(ensure-package-installed 'evil-leader)
(require 'evil)
(require 'evil-common)

;; Use the spacebar as the evil-mode 'leader' key
(require 'evil-leader)
;; Need to activate this before activating evil-mode
(global-evil-leader-mode)
(evil-leader/set-leader "<SPC>")

(ensure-package-installed 'evil)
(evil-mode t)

;; By default evil waits 0.1" after 'Escape' is pressed
(setq evil-esc-delay 0)

(evil-define-command jgy-evil-kill-current-buffer () (kill-buffer nil))
(evil-define-command jgy-evil-save-and-kill-buffer ()
  (progn
    (evil-write nil nil nil nil nil)
    (kill-buffer nil)))

(defun jgy-paste-from-register-0 ()
  (interactive)
  (mark-word)
  (delete-region (region-beginning) (region-end))
  (insert-register ?0 t))

(defun jgy-replace-single-quotes-with-double-quotes (start end)
  "Replace single quotes with double quotes in the current region."
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (goto-char (point-min))
      (while (search-forward "'" nil t) (replace-match "\"" nil t))))

(defun jgy-modernise-ruby-hash (start end)
  "Convert hash rockets to symbol: value"
  (interactive "r")
  (replace-regexp
    "\"\\([a-z_\\-]+\\)\" => \\([^,]+\\)"
    "\\1: \\2"
    nil (point-min) (point-max))
    (goto-char (point-min)))

;;; Set shortcuts: <SPC> + key
(evil-leader/set-key
  "<left>"  'jgy-previous-buffer
  "h"       'jgy-previous-buffer
  "<right>" 'jgy-next-buffer
  "l"       'jgy-next-buffer
  "<SPC>"   'jgy-toggle-buffer
  "a"       'ag
  "b"       'ido-switch-buffer
  "f"       'fiplr-find-file
  "F"       'jgy-fiplr-clear-cache-find-file
  "p"       'jgy-paste-from-register-0
  "q"       'jgy-replace-single-quotes-with-double-quotes
  "r"       'jgy-modernise-ruby-hash)

;; Map :q (and :wq) to commands that only close the current buffer because
;; evil-mode defaults to closing the current window, frame and Emacs
(evil-ex-define-cmd "q[uit]" 'jgy-evil-kill-current-buffer)
(evil-ex-define-cmd "wq" 'jgy-evil-save-and-kill-buffer)

;; Handle my most common typos (upper- for lower-case)
(evil-ex-define-cmd "Q"  'jgy-evil-kill-current-buffer)
(evil-ex-define-cmd "Qa" 'evil-quit-all)
(evil-ex-define-cmd "W"  'evil-write)
(evil-ex-define-cmd "Wq" 'evil-save-and-close)
(evil-ex-define-cmd "WQ" 'evil-save-and-close)

(define-key evil-normal-state-map "\C-p" 'fiplr-find-file)

;;; Set up snippets
(ensure-package-installed 'yasnippet)
(require 'yasnippet)
;; Put my snippets in ~/.emacs.d/snippets
(setq jgy-snippets-dir (expand-file-name "snippets" user-emacs-directory))
(yas-global-mode 1)
(setq yas-snippet-dirs '(jgy-snippets-dir))
