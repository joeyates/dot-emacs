;;;; Set up syntax highlighting and editing behaviour for
;;;; languages and formats.

;;; Note about underscore:
;;; In order to treat '_' as a word character,
;;; the following gets overridden in some cases by mode changes:
(modify-syntax-entry ?_ "w")
;;; so it needs to be set for each major mode that requires it.

;;; All editing
;; Highlight matching elements (parentheses, etc)
(show-paren-mode 1)
;; Never use TAB when indenting
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

;;; Elixir
(ensure-package-installed 'elixir-mode)
(add-hook 'elixir-mode-hook
          (lambda ()
            (progn
              (modify-syntax-entry ?: "'")
              (modify-syntax-entry ?_ "w"))))
(add-to-list 'auto-mode-alist '("\\.livemd\\'" . elixir-mode))
;;; Recognise Elixir script shebang
(add-to-list 'interpreter-mode-alist '("elixir" . elixir-mode))

(add-hook 'html-mode-hook (lambda () (modify-syntax-entry ?_ "w")))

;;; Javascript
(ensure-package-installed 'rjsx-mode)
(add-to-list 'auto-mode-alist '("\\.es6\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.mjs\\'" . rjsx-mode))
(add-hook 'rjsx-mode-hook (lambda () (modify-syntax-entry ?_ "w")))
(setq js2-basic-offset 2)
(setq js2-strict-missing-semi-warning nil)
(with-eval-after-load 'rjsx-mode
  (define-key rjsx-mode-map "<" nil)
  (define-key rjsx-mode-map (kbd "C-d") nil))

;;; JSON
(ensure-package-installed 'json-mode)
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-hook 'json-mode-hook (lambda () (modify-syntax-entry ?_ "w")))
(setq js-indent-level 2)

;;; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook (lambda () (modify-syntax-entry ?- "w")))

;;; Markdown
(ensure-package-installed 'markdown-mode)
(add-hook 'markdown-mode-hook 'whitespace-mode)
(add-hook 'markdown-mode-hook
          (lambda ()
            (progn
              (modify-syntax-entry ?$ ".")
              (modify-syntax-entry ?@ "w")
              (modify-syntax-entry ?_ "w"))))

;;; Nix
(ensure-package-installed 'nix-mode)
(add-to-list 'auto-mode-alist '("\\.nix$" . nix-mode))

;;; Prolog
(add-to-list 'auto-mode-alist '("\\.pl$" . prolog-mode))

;;; Ruby
(ensure-package-installed 'enh-ruby-mode)
(autoload 'enh-ruby-mode "enh-ruby-mode" "Major mode for ruby files" t)
(add-to-list 'auto-mode-alist '("\\.feature$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rb$" . enh-ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake$" . enh-ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))
(setq enh-ruby-bounce-deep-indent nil)
(setq enh-ruby-deep-indent-paren nil)
(setq enh-ruby-deep-indent-construct nil)
(setq enh-ruby-check-syntax t) ; Show syntax errors
(add-hook 'enh-ruby-mode-hook
          (lambda ()
            (progn
              (modify-syntax-entry ?: "'")
              (modify-syntax-entry ?_ "w"))))

(ensure-package-installed 'rust-mode)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;;; SASS

(ensure-package-installed 'sass-mode)
(setq sass-indenti-offset 2)

;;; Shell
(setq sh-basic-offset 2)
(setq sh-indentation 2)
(add-hook 'sh-mode-hook
          (lambda ()
            (progn
              (modify-syntax-entry ?: "$")
              (modify-syntax-entry ?_ "w"))))

;;; Slim
(ensure-package-installed 'slim-mode)
(add-to-list 'auto-mode-alist '("\\.slim\\'" . slim-mode))
(add-to-list 'auto-mode-alist '("\\.slime\\'" . slim-mode))
(add-to-list 'auto-mode-alist '("\\.slive\\'" . slim-mode))

;;; Typescript
(ensure-package-installed 'typescript-mode)
(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
(setq typescript-indent-level 2)
