;;;; Settings which change Emacs' display

(ensure-package-installed 'solarized-theme)
(load-theme 'solarized-dark t)

(require 'whitespace)
;;; Change fore- and background at the 80th column
(setq whitespace-line-column 80) ;; limit line length
;; https://www.emacswiki.org/emacs/WhiteSpace#toc7
(setq whitespace-style '(empty face lines-tail tabs trailing))
(add-hook 'prog-mode-hook 'whitespace-mode)
(set-face-attribute 'whitespace-trailing nil :background nil :foreground "gray30")
(set-face-attribute 'whitespace-empty nil :background nil :foreground "gray30")

;; Show current line **and** column
(setq column-number-mode t)

;; Format Emacs' modeline
(setq-default
 mode-line-format
 '(
   ;; Modified?
   (:propertize mode-line-modified  face (:weight normal))
   " "
   ;; Git repo information
   ;; (:propertize (vc-mode vc-mode) face (:weight normal))
   ;; The file name:
   (:propertize mode-line-buffer-identification face (:weight bold))
   " "
   ;; Position in file: `Top|n%|Bottom (line,colum)`
   (:propertize mode-line-position face (:weight normal))
   " "
   ;; list of current modes
   mode-line-modes
   mode-line-misc-info
   mode-line-end-spaces))
