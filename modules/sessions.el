;;;; Save the list of open files to ./.emacs.desktop

(require 'desktop)

;; Skip loading/saving desktop for 'edit no session'
(unless (getenv "EMACS_NO_SESSION")
  (progn
    ;; Save desktop in the current directory
    (add-to-list 'desktop-path ".")
    ;; Save desktop without asking confirmation
    (setq desktop-save t)
    (desktop-save-mode 1)
  )
)
