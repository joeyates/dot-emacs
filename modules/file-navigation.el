; install fiplr + grizzl
(ensure-package-installed 'fiplr)
(setq fiplr-ignored-globs
      '((directories (".git" "auto-save-list" "_build" ".chef" "cookbooks" "deps" "dist" "elpa" "node_modules" "static" "rel" "temp" "tmp" "vendor"))
  (files ("*.pyc" ".emacs.desktop" ".emacs.desktop.lock"))))

(defun jgy-fiplr-clear-cache-find-file ()
  (interactive)
  (fiplr-clear-cache)
  (fiplr-find-file))

;;; Configure ag/SilverSearcher
(ensure-package-installed 'ag)
(setq ag-highlight-search t)
(setq ag-reuse-buffers 't) ; Just one results buffer
(setq ag-arguments ()) ; Disable '--smart-case'  and --stats
