;;;; Change some annoying emacs defaults

;; Suppress startup screen
(setq inhibit-startup-message t)
;; Start with an empty scratch buffer
(setq initial-scratch-message nil)
;; Hide the menu
(menu-bar-mode -1)
;; Don't make backups of edited files
(setq make-backup-files nil)
;; Don't wrap text
(set-default 'truncate-lines t)
;; Accept y/n for confirmation
(defalias 'yes-or-no-p 'y-or-n-p)
;; Allow lowercase UTF-8 encoding
(define-coding-system-alias 'UTF-8 'utf-8)
