;;;; I've tried out helm and helm-projectile and they don't do what I want

;;; Functions used to switch between Emacs' buffers

(defun jgy-ignorable-buffer (name)
  "Returns true for buffer names I don't want to visit."
  (or
    (string-match-p "^\\*" name)
    (string-match-p "^ \\*" name)))

;;; Derived from
;;; http://emacs.stackexchange.com/questions/17687/make-previous-buffer-and-next-buffer-to-ignore-some-buffers/17694#17694
(defun jgy-change-buffer (change-buffer)
  "Call CHANGE-BUFFER until `jgy-ignorable-buffer' returns false for the current buffer."
  (let ((initial (current-buffer)))
    (funcall change-buffer)
    (let ((first-change (current-buffer)))
      (catch 'loop
  (while (jgy-ignorable-buffer (buffer-name))
    (funcall change-buffer)
    (when (eq (current-buffer) first-change)
      (switch-to-buffer initial)
      (throw 'loop t)))))))

(defun jgy-next-buffer ()
  "Variant of `next-buffer' that skips buffers matching `jgy-ignorable-buffer'."
  (interactive)
  (jgy-change-buffer 'next-buffer))

(defun jgy-previous-buffer ()
  "Variant of `previous-buffer' that skips buffers matching `jgy-ignorable-buffer'."
  (interactive)
  (jgy-change-buffer 'previous-buffer))

;;; http://www.emacswiki.org/emacs/SwitchingBuffers
(defun jgy-toggle-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

;;; Set up ido for buffer switching

(ensure-package-installed 'ido)
(require 'ido)
(ido-mode t)
;; Enable fuzzy matching
(setq ido-enable-flex-matching t)
;; Set up buffers I never want to switch to
(setq ido-ignore-buffers (list #'jgy-ignorable-buffer))
